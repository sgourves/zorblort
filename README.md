# Zorblort

## Description

Incroyable projet pour parler avec Zorblort !!!
Il est trop fort ce Zorblort !!

## Researches

Show system information: `inxi -Fxz`.

### Kubernetes (legacy mode)

Voir ce lien pour la configuration : https://medium.com/@yuxiaojian/host-your-own-ollama-service-in-a-cloud-kubernetes-k8s-cluster-c818ca84a055

Usage, tests en local :

```bash
k apply -f kubernetes/zorblort-namespace.yml
k apply -f kubernetes/ollama.yml
k apply -f kubernetes/webui.yml
```

### Helm integration

Tutorial link: [Create Helm Chart](https://devopscube.com/create-helm-chart)

Commands:

```bash
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
helm version

helm create zorblort-chart
```
